<?php $region = $block->region;
$theme_path = base_path() . path_to_theme();
$sidebar_region=$region=='sidebar_first' || $region=='sidebar_second' || $region=='headerabovecolumn1' || $region=='headerabovecolumn2' || $region=='headerabovecolumn3' || $region=='headerabovecolumn4' || $region=='headerbelowcolumn1' || $region=='headerbelowcolumn2' || $region=='headerbelowcolumn3' || $region=='headerbelowcolumn4' || $region=='slideshowabovecolumn1' || $region=='slideshowabovecolumn2' || $region=='slideshowabovecolumn3'|| $region=='slideshowabovecolumn4' || $region=='slideshowbelowcolumn1' || $region=='slideshowbelowcolumn2' || $region=='slideshowbelowcolumn3' || $region=='slideshowbelowcolumn4' ||$region=='menuabovecolumn1' || $region=='menuabovecolumn2' || $region=='menuabovecolumn3' || $region=='menuabovecolumn4' || $region=='menubelowcolumn1' || $region=='menubelowcolumn2' || $region=='menubelowcolumn3' || $region=='menubelowcolumn4' || $region=='contenttopcolumn1' || $region=='contenttopcolumn2' || $region=='contenttopcolumn3' || $region=='contenttopcolumn4' ||$region=='contentbottomcolumn1' || $region=='contentbottomcolumn2' || $region=='contentbottomcolumn3' || $region=='contentbottomcolumn4' ||$region=='footerabovecolumn1'  || $region=='footerabovecolumn2'|| $region=='footerabovecolumn3' || $region=='footerabovecolumn4' ||$region=='footerbelowcolumn1' || $region=='footerbelowcolumn2' || $region=='footerbelowcolumn3' || $region=='footerbelowcolumn4' || $region=='leftfooterarea' || $region=='centerfooterarea' || $region=='rightfooterarea';
if($block->module=="system" && ($block->delta=="user-menu"|| $block->delta=="main-menu"||$block->delta=="navigation"||$block->delta=="management")){?>
 <?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
<div id="<?php print $block_html_id;?>"class="ttr_verticalmenu <?php print $classes;?>"<?php print $attributes;?> >
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_verticalmenu_header">
<h3 <?php print $title_attributes; ?> class="ttr_verticalmenu_heading">
<?php print $block->subject ?>
</h3>
 <?php print render($title_sufix); ?>
<?php else: ?>
<div id="<?php print $block_html_id;?>"class="ttr_verticalmenu <?php print $classes;?>"<?php print $attributes;?> >
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_verticalmenu_without_header">
<?php endif;?>
</div>
<div class="ttr_verticalmenu_content">
<ul class="ttr_vmenu_items nav nav-pills nav-stacked">
<?php echo user_menu_generate_menu("ttr_",$block->delta);?>
</ul>
</div>
</div>
<?php }
elseif($block->module=="menu"){?>
 <?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
<div id="<?php print $block_html_id;?>"class="ttr_verticalmenu <?php print $classes;?>"<?php print $attributes;?> >
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_verticalmenu_header">
<h3 <?php print $title_attributes; ?> class="ttr_verticalmenu_heading">
<?php print $block->subject ?>
</h3>
 <?php print render($title_sufix); ?>
<?php else: ?>
<div id="<?php print $block_html_id;?>"class="ttr_verticalmenu <?php print $classes;?>"<?php print $attributes;?> >
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_verticalmenu_without_header">
<?php endif;?>
</div>
<div class="ttr_verticalmenu_content">
<ul class="ttr_vmenu_items nav nav-pills nav-stacked">
<?php echo user_menu_generate_menu("ttr_",$block->delta);?>
</ul>
</div>
</div>
<?php }
else { if($sidebar_region): ?>
<?php if($block->region == 'sidebar_first'):?>
<div class="ttr_sidebar_left_padding <?php print $classes;?>"<?php print $attributes;?>>
<?php else:?>
<div class="ttr_sidebar_right_padding <?php print $classes;?>"<?php print $attributes;?>>
<?php endif;?>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
 <?php if ($block->subject): ?>
<div id="<?php print $block_html_id;?>"class="ttr_block">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_block_header">
<?php print render($title_prefix); ?>
<h3 class="ttr_block_heading">
<?php print $title_attributes; ?>
<?php print $block->subject ?>
</h3>
 <?php print render($title_suffix); ?>
<?php else: ?>
<div id="<?php print $block_html_id;?>"class="ttr_block">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="ttr_block_without_header">
<?php endif; ?>
</div>
<div class="ttr_block_content" <?php print $content_attributes;?> >
<?php print $content ?>
</div>
</div>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
</div>
<?php else :?>
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>><?php print render($title_prefix); ?> <?php if ($block->subject): ?> <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2><?php endif;?> <?php print render($title_suffix); ?> <div class="content"<?php print $content_attributes; ?>><?php print $content ?></div></div>
<?php endif?><?php } ?>
