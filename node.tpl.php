<?php
global $columncounter;
if(drupal_is_front_page())
{
$lg =4;
$class_suffix_lg  = round((12 / $lg));
 $md =4;
$class_suffix_md  = round((12 / $md));
 $xs =1;
$class_suffix_xs  = round((12 / $xs)); ?>
<div class="col-lg-<?php echo $class_suffix_lg;?> col-md-<?php echo $class_suffix_md;?> col-sm-<?php echo $class_suffix_md;?> col-xs-<?php echo $class_suffix_xs;?>">
<?php } ?>
<article id="node-<?php print $node->nid; ?>" class="ttr_post <?php print $classes; ?>">
<div class="ttr_post_content_inner">
<div class="ttr_post_inner_box">
<?php print render($title_prefix); ?>
<h2 class="ttr_post_title">
<?php $theme_path = base_path() . path_to_theme(); ?>
<a href="<?php print $node_url; ?>">
<?php print $title; ?>
</a>
</h2>
<?php print render($title_suffix); ?>
</div>
<div class="ttr_article">
<?php if ($display_submitted): ?>
<?php endif; ?>
<div  class="postcontent"<?php print $content_attributes; ?>>
<?php show ($content['comments']); ?>
<?php  show ($content['links']); ?>
<?php print render($content); $columncounter++;?>
<div style="clear:both;"></div>
</div>
</div>
</div>
</article>
</div>
<?php if(drupal_is_front_page())
{
 if(($columncounter) % $xs == 0){
echo '<div class="clearfix visible-xs-block"></div>';}
if(($columncounter) % $md == 0){ 
echo '<div class="clearfix visible-sm-block"></div>';
echo '<div class="clearfix visible-md-block"></div>';}
if(($columncounter) % $lg == 0){ 
echo '<div class="clearfix visible-lg-block"></div>';}
} ?>
